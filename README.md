### SpringCloud 微服务实践




> 参考资料：http://blog.csdn.net/forezp/article/details/70148833
>          https://springcloud.cc/
  
1. eureka-server      服务注册中心                   端口号：9001
1. config-server      分布式配置中心                 端口号：9101
1. config-client      配置中心调用客户端             端口号：9199
1. service-hi         服务提供者                     端口号：8001
1. service-ribbon     服务消费者-负载均衡器          端口号：8101
1. service-feign      服务消费者(声明式，集成ribbon) 端口号：8201
1. service-zuul       路由网关                       端口号：8301