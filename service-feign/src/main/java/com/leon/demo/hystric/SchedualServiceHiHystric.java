package com.leon.demo.hystric;

import com.leon.demo.interfaces.SchedualServiceHi;
import org.springframework.stereotype.Component;

/**
 * Created by 陈明磊 on 2017/8/21.
 */
//实现SchedualServiceHi 接口，并注入到Ioc容器中
@Component
public class SchedualServiceHiHystric implements SchedualServiceHi {
    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry "+name;
    }
}
