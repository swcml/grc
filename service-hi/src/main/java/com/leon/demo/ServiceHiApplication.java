package com.leon.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 创建一个服务提供者 (eureka client)
 */
@EnableEurekaClient //添加 @EnableDiscoveryClient 或 @EnableEurekaClient 注解到启动类上，将自己注册到 Erueka Server。
@SpringBootApplication
public class ServiceHiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceHiApplication.class, args);
	}
}
